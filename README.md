&emsp;



> &#x1F34E; 连享会 - 生存分析专题 (Survival Analysis)   

 
> &#x1F34F; 课程大纲：[网页版](https://gitee.com/arlionn/st/blob/master/%E8%BF%9E%E4%BA%AB%E4%BC%9A-%E7%94%9F%E5%AD%98%E5%88%86%E6%9E%90%E4%B8%93%E9%A2%98-%E7%AE%80%E4%BB%8B.md) ；  PDF 版：[V1](https://gitee.com/arlionn/st/raw/master/PDF_%E8%BF%9E%E4%BA%AB%E4%BC%9A-%E7%8E%8B%E5%AD%98%E5%90%8C%E6%95%99%E6%8E%88-%E7%94%9F%E5%AD%98%E5%88%86%E6%9E%90%E4%B8%93%E9%A2%98-2020.6.6.pdf) &emsp; [V2](https://quqi.gblhgk.com/s/880197/DmHRC3s9U1x5ldTo)     

> &#x2753;  [常见问题解答 - FAQs](https://gitee.com/arlionn/st/wikis/FAQs.md?sort_id=2276379)

> &#x1F449; [相关课程](https://gitee.com/arlionn/Course)


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/背景-苔藓001.jpg)

---





## 课程概览

- **简介：** 本课程介绍实证研究中常见的生存分析方法和模型，包括：描述性统计 (K-M 估计)、离散时间 Logit 模型、参数模型、分段恒定对数比率模型、比率风险模型等。
- **课程主页：** <https://gitee.com/arlionn/st>
- **授课方式：网络直播**，缴费后主办方发送邀请码点击邀请码即可进入在线课堂，收看直播无需安装任何软件。
- **时间：** 2020 年 6 月 6 日 (周六)，已结束，您可以收藏或【Fork】本仓库，以便获取后续课程信息。 
- **授课嘉宾：** [王存同](http://ssp.cufe.edu.cn/info/1127/3289.htm) 教授 (中央财经大学)
- **软件/课件：** Stata，提供全套 Stata 实操程序、数据和 dofiles (开课前一周发送)。建议使用 Stata 13.0 或更高版本，建议软件购买地址：[Stata 官网](http://www.stata.com/info/order/new/edu/gradplans/gp2-order.html) <http://www.stata.com>。
  - 课程大纲中涉及的参考文献下载链接：https://pan.baidu.com/s/149eF4l6GczGsaFiVII875g ；提取码：8mth 






&emsp;


> ## 相关课程

&emsp;

> #### [连享会 - 效率分析专题](https://www.lianxh.cn/news/a94e5f6b8df01.html)     
> **已上线**：可随时购买学习+全套课件，[课程主页](https://gitee.com/arlionn/TE) 已经放置板书和 FAQs   
> 主讲嘉宾：连玉君 | 鲁晓东 | 张宁      
> [课程主页](https://gitee.com/arlionn/TE)，[微信版](https://mp.weixin.qq.com/s/zRotMGebIQqaMih8B71fdg) &ensp; <https://gitee.com/arlionn/TE>

![连享会-效率分析专题视频](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/TE专题_连享会_lianxh.cn700.png "连享会-效率分析专题视频，三天课程，随时学")

&emsp; 



&emsp;

> #### [连享会 - 文本分析与爬虫 - 专题视频](https://www.lianxh.cn/news/88426b2faeea8.html)    
> 主讲嘉宾：司继春 || 游万海

![连享会-文本分析与爬虫-专题视频教程](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lanNew-文本分析-海报002.png "连享会-文本分析与爬虫-专题视频，四天课程，随随时学")



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)


&emsp; 

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| **[效率分析-专题](https://gitee.com/arlionn/TE)** | 连玉君<br>鲁晓东<br>张&emsp;宁 | [视频-TFP-SFA-DEA](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线 |
| **文本分析/爬虫** | 游万海<br>司继春 | [视频-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线 |
| **二元选择模型** | 司继春 | [直播：二元选择与计数数据](https://www.lianxh.cn/news/91c3fc21cdbbc.html) |
| **分位数回归** | 游万海 | [视频，2小时，88元](https://lianxh.duanshu.com/#/brief/course/f0bfb3102ada48969966c92123a7ebf0) |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 论文重现 | 连玉君    | [经典论文精讲](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)，[-课程资料-](https://gitee.com/arlionn/Paper101), [-幻灯片-](https://quqi.com/s/880197/nz9LvbzzEEpWBNrD)   |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |
| 大数据 | 李兵 | [经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26) |
| R 语言 | 游万海 | [R 语言初识](https://lianxh.duanshu.com/#/brief/course/a719037536de4812a630a599f8cd7b43), 9.9元
| Python+Github | 司继春 | [Python和Github入门](https://lianxh.duanshu.com/#/brief/course/132a12b2e5ef45b795bfa897c037a6f4) <br> 2小时, 9.9元

> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">


